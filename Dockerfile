FROM gcc:8.1

ENV REFRESHED_AT 2019-02-13

RUN apt-get update \
    && apt-get -y install git scons bzr lib32z1 lib32ncurses5 lcov

# Set up a tools dev directory
WORKDIR /home/dev

# pull the gcc-arm-none-eabi tarball
RUN wget https://developer.arm.com/-/media/Files/downloads/gnu-rm/8-2018q4/gcc-arm-none-eabi-8-2018-q4-major-linux.tar.bz2 \
    && tar xvf gcc-arm-none-eabi-8-2018-q4-major-linux.tar.bz2 \
    && rm gcc-arm-none-eabi-8-2018-q4-major-linux.tar.bz2

# Set up the compiler path
ENV PATH $PATH:/home/dev/gcc-arm-none-eabi-8-2018-q4-major/bin

WORKDIR /usr/project
